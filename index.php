<!DOCTYPE html>
<html>
<meta charset="UTF-8">
<link rel="stylesheet" href="css.css" type="text/css">

<body>

    <?php
    $khoas = array("" => '', "MAT" => 'Khoa học máy tính', "KDL" => 'Khoa học dữ liệu');
    // $sql_1 = "SELECT  *  FROM student WHERE faculty='".$faculty."'";
    $conn = new mysqli('localhost', 'root', '', 'web');
    session_start();
    $db_name = (isset($_SESSION["name"])) ? $_SESSION["name"]:"";
    $db_faculty  = (isset($_SESSION["faculty"])) ? $_SESSION["faculty"]:"";
    session_destroy();

    $sql = "SELECT * FROM student";
    if(empty($db_name) && !empty( $db_faculty)){
        $sql = "SELECT  *  FROM student WHERE faculty='".$db_faculty."'";
    }else if(!empty($db_name) && empty( $db_faculty)){
        $sql = "SELECT  *  FROM student WHERE name LIKE '%".$db_name."%'";
    }else if(!empty($db_name) && !empty( $db_faculty)){
        $sql = "SELECT  *  FROM student WHERE faculty='".$db_faculty."' AND name LIKE '%".$db_name."%'";
    }
    $result = $conn->query($sql);

    if (!empty($_POST["submit"])) {
        session_start();
        $_SESSION["name"] = $_POST["tukhoa"];
        $_SESSION["faculty"] = $_POST["khoa"];
        header('Location: index.php');
    }

    ?>
    <form method="POST" enctype='multipart/form-data'>
        <div class="bg">
            <div class="list_div">
                <div class="row_index">
                    <div class="column_label div_tab ">Khoa</div>
                    <div class="column_input_index ">
                        <select id="chonKhoa" class="div_select_khoa" name="khoa">
                        <?php
                            foreach ($khoas as $khoa => $value) { { ?>
                                    <option value=<?= $khoa ?> <?= (isset($_POST['khoa']) && $_POST['khoa'] == $khoa) ? 'selected' : ''; ?>><?php echo $value ?></option>
                            <?php }
                            };
                            ?>
                        </select>
                    </div>
                </div>

                <div class="row_index">
                    <div class="column_label div_tab">Từ khóa</div>
                    <div class="column_input_index">
                        <input class="div_input_tukhoa" id="tukhoa" name="tukhoa" value='<?php if (isset($_POST['tukhoa']) && $_POST['tukhoa'] != NULL) {
                                                                                                echo $_POST['tukhoa'];
                                                                                            } ?>'>
                    </div>
                </div>

                <div class="">
                    <input class="btn_ div_button_index" readonly value="Xóa" id="delSearch" onclick="clearSearch()">
                    <input class="btn_ div_button_index" type="submit" value="Tìm kiếm" name="submit">
                </div>


            </div>

            <div class="div_table">
                <table class="table_0" style="width:100%">
                    <tr>
                        <td colspan=3>Số sinh viên tìm thấy: <?php echo($result->num_rows) ?></td>
                        <td width="25%">
                            <a href="register.php" class="btn_add" style="text-decoration:none"> Thêm </a>
                        </td>
                    </tr>

                    <tr>
                        <td>No</td>
                        <td>Tên sinh viên</td>
                        <td>Khoa</td>
                        <td style=" text-align:center">Action</td>
                    </tr>
                    <?php
                    if ($result->num_rows > 0) {
                        while ($row = $result->fetch_assoc()) {
                            $faculty = ( $row["faculty"]== "MAT") ? 'Khoa học máy tính' : 'Khoa học dữ liệu';
                            echo'
                            <tr>
                                <td width="2%" style=" text-align:right">'.$row["id"].'</td>
                                <td width="45%">'.$row["name"].'</td>
                                <td width="28%">'.$faculty.'</td>
                                <td style=" text-align:center" width="25%">
                                    <div>
                                        <input class=" btn_change_delete" type="submit" value="Xóa" name="delete">
                                        <input class=" btn_change_delete" type="submit" value="Sửa" name="change">
                                    </div>
                                </td>
                            </tr>
                            ';
                        }
                    }
                    $conn->close();
                    ?>
                </table>
            </div>
        </div>
    </form>


</body>

<script>
    function clearSearch() {
        document.getElementById("delSearch").addEventListener('click', function() {
            document.getElementById("tukhoa").value = "";
            document.getElementById("chonKhoa").value = "";
        });

    }
</script>